EAL-ITT admin (2019 UCL-ITT)
================

Projekt til administrative dokumenter og links, som ikke er klasse/årgangsspecifikke


Detaljer om serverrummet, ip adresser, mv kan findes [her](https://gitlab.com/ucl-juniperlab/admin/tree/master/) 

Seneste [fou projekt dokument](https://gitlab.com/EAL-ITT/admin/-/jobs/artifacts/master/file/fou/Projekter_til_FoU.pdf?job=build+pdfs)
