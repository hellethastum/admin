---
title: 'IT Teknolog - FoU'
subtitle: 'Projekt forslag'
authors: ['Nikolaj Simonsen \<nisi@ucl.dk\>', 'Per Dahlstrøm \<pda@ucl.dk\>', 'Ilias Esmati \<iles@ucl.dk\>']
main_author: 'Nikolaj Simonsen'
date: \today
email: 'nisi@ucl.dk'
left-header: \today
right-header: 'IT Teknolog - FoU - Projekt forslag'
---


# Introduktion
====================

## Fomål med dette dokument

At skitsere den type af projekter IT Teknologerne kan arbejde med inden for FoU og sammen med studerende på uddannelsen.

## Målgruppe

IT Teknolog ledelsen og UCL FoU afdelingen.

## Overordnet beskrivelse for alle projekter på IT Teknolog uddannelsen.

## Fra Studieordningen forår 2018.

Fagelementet skal medvirke til, at den studerende opnår færdigheder og tilegner sig ny viden inden for projektledelse af IT Teknolog orienterede projekter.  
Den studerende følger en given projektstyringsmodel, gerne med afsæt i et virksomhedsprojekt.  

## Grundlæggende læringsmål for projekterne på 1. 2. og 3. Semester

### Viden
Den studerende har viden om og forståelse for:  
* En relevant projektstyringsmodel i forbindelse med udviklingsprojekter indenfor IT    
* En relevant systemudviklingsmetode i forbindelse med udviklingsprojekter indenfor IT  
* Har viden om forskellige projektstyringsmetoder indenfor IT udvikling
* Har viden om systemudviklingsmetoder


### Færdigheder

Den studerende kan:  
* Anvende en projektstyringsmetode i forbindelse med udviklingsprojekter indenfor IT
* Anvende viden, metoder og værktøjer i forbindelse med design, udvikling og test af
produkt eller systemer
* Integrere teknologier fra valgfag i forbindelse med udviklingsprojekt


### Kompetencer

Den studerende kan:
* Håndtere samspillet mellem givne teknologier i integrerede løsninger
* Håndtere planlægning og kvalitetsstyring af egne tekniske opgaver
* Selvstændigt udvælge og anvende en projektstyringsmetode i forbindelse med
udviklingsprojekter inden for IT
* Selvstændigt anvende viden, metoder og værktøjer i forbindelse med design, udvikling og
test af produkt eller systemer
* Selvstændigt integrere teknologier fra valgfag
* Håndtere samspillet mellem hardware, software og netværk i integrerede løsninger
* Selvstændigt håndtere planlægning og kvalitetsstyring af egne tekniske opgaver
* I struktureret sammenhæng tilegne sig ny viden, færdigheder og kompetencer indenfor fagområdet.


## 1. Semester Projektet 

Her gives en beskrivelse af IT Teknolog underviserteamets implementering af ovenstående læringsmål:  


### Beskrivelse

Projektet har et højt stilladseringsniveau (det vil sige, at underviserne i høj grad levere styringen og struktureringen af projektet) og præsenterer den studerende for et basis IoT system.  
Systemet består af simple elektroniske sensorer, embedded systemer og netværks konfiguration.   
Målet er at opbygge et IoT system med 2 vejs end-to-end kommunikation.  
Projektet introducerer de studerende for relevante digitale projektstyringsværktøjer.   
De studerende skal dokumentere og offentliggøre deres resultater med henblik på at kunne udstille egne evner for virksomheder de senere skal søge praktik og ansættelse hos.  

**Eksempel**  
Nedenstående blokdiagram viser princippet i et system som en IT Teknolog kan designe.  
Via et eller flere netværk og forbindelser af en eller anden type/protokol, kan Device A kommunikere med Device E i begge retninger.  
Det vil sige at Device A f.eks. kan udgøre en central styreenhed som opsamler data fra Device E og også kan give signaler til Device E som så kan påvirke et fysisk system på baggrund af de opsamlede data. Dvs. Device E repræsenterer både en sensor og en aktuator.

Systemets kompleksitet kan f.eks. øges med anvendelse af cloud og et stort antal sensorer og aktuatorer..  
Anvendelsesmulighederne er nærmest uendelige: Robotter, robot supportsystemer, autonome lagersystemer, vejrstation, procesanlæg og så videre:

## 2. Semester projektet

### Beskrivelse

Projektet omhandler et IoT system med øget kompleksitet i forhold til 1. Semester projektet.  
Stilladseringen er mindre end 1. Semester projektet og de studerende skal i større grad projektstyre og planlægge eget arbejde.  
Projektet udarbejdes i forhold til en virksomhedscase og de studerende skal tilpasse deres produkt til de krav som virksomheden sætter.   
De studerende skal dokumentere og offentliggøre deres resultater med henblik på at kunne udstille egne evner for virksomheder de senere skal søge praktik og ansættelse hos.   

### Eksempel  

Hydac A/S ønsker at udleje hydraulik pumper til kunder og afregne for reelt forbrug. For at kunne det, ønsker de en IoT løsning med interface til deres eksisterende sensorer (tryk, rpm etc.) og industri standarder (4-20mA, 0-5V etc.).  
De studerende arbejder med en microprocessor platform der kommunikerer forbrugsdata via GSM til en cloud tjeneste. De studerende udvikler også værktøjer til at teste og diagnosticere IoT løsningen.  

## 3. Semester projektet


### Beskrivelse

De studerende samarbejder med en virksomhed og tidsstyrer selv projektet.  
De studerende har mulighed for at vælge mellem flere forskellige virksomheds cases.  
Indholdet af projektet skal være relevant i forhold til de faglige elementer fra studieordningen.  
De studerende skal dokumentere og offentliggøre deres resultater med henblik på at kunne udstille egne evner for virksomheder de senere skal søge praktik og ansættelse hos.  

Nuværende virksomheder og projekter vi ser som potentielt interessante for vores 3. semester projekter:


### Vestas 
Vi er i dialog   
### Linak
Vi er i dialog   
### Nomotech
1. The project aims to develop a purely hardware-based system that can detect and act on a peak in the analog signal from a joystick, which is a gesture by the user that signals a request to change a variable.
2. The project aims to develop a purely hardware-based system to manipulate a linear analog signal to have two exponential manipulators depending on whether the signal is going negative or positive compared to center position.

### Itelligence danmark
Udvikling af et intelligent gæstekort der anvender et indlejret system til at kommunikere via wifi med virksomhedens backend systemer samt tegne grafik på et “e-ink” display.
På displayet vises gæstens informationer samt status om parkerings udløb, QR kode til kantine etc.

### FOU 
Development of pendular robotic platforms for teaching control systems and IoT
Combining engineering concepts, programming and mathematics, teaching control systems can quite often be seen as a challenge, and can be perceived by students as quite theoretical. For this reason, it is very important to have hands-on experience so that students get a good feeling about PID control, widely used in the industrial world. In this project, we will develop small pendular robotic platforms that will contain the basic elements of a control system (sensor, actuator, controller) so that the main elements of control engineering have a concrete and practical meaning.
Itelligence - smart visitor tag

### Hydac A/S 

* 2. Iteration af det projekt vi har på 2. semester

        [https://eal-itt.gitlab.io/iot-with-particle/](https://eal-itt.gitlab.io/iot-with-particle/)

### 4. Semester - Det afsluttende projekt


Det afsluttende eksamensprojekt skal dokumentere den studerendes forståelse af praksis og central anvendt teori og metode i relation til en praksisnær problemstilling, der tager udgangspunkt i en konkret opgave inden for uddannelsens område.  
Problemstillingen, der skal være central for uddannelsen og erhvervet, formuleres af den studerende, eventuelt i samarbejde med en privat eller offentlig virksomhed.  
Institutionen godkender problemstillingen.