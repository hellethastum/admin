---
title: 'IT Technology Team Meeting Agenda'
subtitle: 'Agenda'
authors: ['Per Dahlstrøm \<pda@ucl.dk>']
main_author: 'Per Dahlstrøm'
date: \today
email: 'pda@ucl.dk'
left-header: \today
right-header: 'IT Technology Team Meeting Agenda'
---

# Reader
Students and lectureres and management.

---

# Purpose

1. Call for class representatives to participate in ITT Team lecturer meeting
2. Prepare class representative for the meeting.

---

<div style="page-break-after: always; visibility: hidden"> 
\pagebreak 
</div>

2020-02-25 Kl.: 13:00 til 13:30. Room B1.40. ITT Teammødeagenda. NISI, RUTR, ILES, MON, PDA

# class representatives:  

1.
*	2. sem. A: Jens VandeVeire	jens365y@edu.ucl.dk  
Substitute: Nicolai Svarer	Skytte	nico835t@edu.ucl.dk
* 2. sem. B:   
Substitut: Nikita	Morozovs	niki1396@edu.ucl.dk
* 4. Sem. A: Nicolai Lyngs Jensen, nico429s@edu.ucl.dk  
Substitute
* 4. Sem. B:  
Substitute: Daniel Mohr Maslygan Lilleballe, dani7378@edu.ucl.dk

2.	Minutes from laste meeting.  
Output: A liste of things hat must be followed further up on.  
*	PDA: Status.  
 

Invitation to the meeting:

Team meeting with the lecturer team and if required the head of department

Dear class representative.

On Tuesday 25 February at 13:00 in room B1.40, there will be a so-called lecturer team meeting (possibly including the head of department if this is needed.)

You are here by invited to participate in the first 20 minutes of the meeting. You will thus have approximately 6 minutes to present matters important to your class and get some answers. It is short time, so please be well-prepared. If more time is needed, we will have to schedule a new meeting with you.

Please have a session, before the above date, with the class where you collect matters of importance to the class to bring to the lecturer team and head of department.
If there is something, you think/know we will have to prepare in order to “answer” you, please feel free to send your questions/matters in advance.

Please confirm you participation asap.

In case you cannot participate, then please make the substitute take over and confirm participation to me.

