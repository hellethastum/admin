---
title: 'IT Technology Team Meeting Agenda'
subtitle: 'Agenda'
authors: ['Per Dahlstrøm \<pda@ucl.dk>']
main_author: 'Per Dahlstrøm'
date: \today
email: 'pda@ucl.dk'
left-header: \today
right-header: 'IT Technology Team Meeting Agenda'
---

# Læsere

Undervisere og ledere.

---

# Formål

1. At opliste de opgaver der skal varetages og opliste emner der skal gennemsnakkes som grundlag for beskrivelse og evt. uddelegering af opgaver der skal varetages.  
2. Danne grundlag for et referat der oplister opgaver der skal varetages med en deadline og en ansvarlig.

---

<div style="page-break-after: always; visibility: hidden"> 
\pagebreak 
</div>

2020-02-xx Kl.: 10:00 til 13:30. ITT Teammødeagenda. NISI, RUTR, ILES, MON, PDA

# Klasserepræsentanter: Bliver indkaldt.  

1.
*	2. sem. A: Jens VandeVeire	jens365y@edu.ucl.dk  
Substitute: Nicolai Svarer	Skytte	nico835t@edu.ucl.dk
* 2. sem. B:   
Substitut: Nikita	Morozovs	niki1396@edu.ucl.dk
* 4. Sem. A: Nicolai Lyngs Jensen, nico429s@edu.ucl.dk  
Substitute
*	4. Sem. B:  
Substitute: Daniel Mohr Maslygan Lilleballe, dani7378@edu.ucl.dk

2.	Referat fra sidste møde  
Output: Beslutninger der skal følges op på.  
*	PDA: Status på beslutninger og beslutninger der skal følges op på:  
    *  	Lecture plans  
    *	Employability – Jobparathed  
    *	Klasserne og Obligatoriske LæringsAktiviteter  
    *	Eksamen juni 2020 Eksamen ITT2 og ITT4  
    *	Vi skal bemande åbent hus lørdag d. 22. februar fra 11:00 til 15:00.  
    *	Studerende der har været til samtale PIEC deltager måske.  
    *  	Budgettal for studerende 2019 efterår til 2020 efterår. Dog ikke september optaget 2020.
    *	Sammenholde lecture fagplaner for koordinering  
    *	Status på praktik  
    *	Projekter i foråret 2020  
    *	Team budget  
    *	IoT virksomheder  
    *	Team corner ~ vores nye pladser i den renoverede lærerlounge.  

3.	Hvad er materialebeløbet på uddannelsen?  
Output: Vi skal have skrevet det samlede materialebeløb på ucl.dk incl. Bøger.  
•	PDA: Hver især gør op hvad der skal bruges i deres fag.  
•	PDA: Får det samlede beløb skrevet på ucl.dk.  

4.	Lecture plans  
OutPut: Konsesus om hvad Lecture Plan og Weekly plan skal indeholde.  
•	Alle: Hvad er kravene? Opfylder vi kravene?

6.	Klasserne og Obligatoriske LæringsAktiviteter.  
Output: Anbefaling til Marie. OLAer eller ikke OLAer.  
•	PDA: Ifølge Pierre er det ikke obligatorisk at køre OLAer.
Giver OLAer nok værdi ifht. indsatsen. Kan vi gøre noget andet?

7.	Eksamen juni 2020 Eksamen ITT4  
Output: Plan for uge 25 og uge 26 for ITT4  
•	ALLE: Orientering.

8.	Vi skal bemande åbent hus lørdag d. 22. februar fra 11:00 til 15:00.  
Output: Plan for aktiviteter og bemanding.  
•	PDA: Orientere om konceptet.

9.	Klima i 1. og 2. sem. klasserne  
Output: Konsensus om hvordan vi agerer og omtaler studerende i klasserne.  
•	ALLE: Input til underviser code of conduct. A og B hold.  

10.	Studerende der har været til samtale PIEC deltager måske.  
Output: Plan for videre arbejde med disse studerende?  
•	NISI/ILES: Orienterer  


11.	Budgettal for studerende 2019 efterår til 2020 efterår. Dog ikke september optaget 2020.  
Output: Opdaterede tal. Tror vi (stadig) på tallene? ~ Korrektioner?  
oeait18eiA + B:  
•	Der er 28 i alt i de to klasser ITT3. Vi regner med at 25 kommer videre fra 3. semester til 4. semester.  
Forventet beståen af 1. års prøven 1. halvdel i januar 2020:  
•	Oeait19eiA: 26  
•	Oeait19eiB: 26  
	Forventet beståen af 1. års prøven i juni 2020:  
•	Oeait19eiA: 18  
•	Oeait19eiB: 18  
	Forventet beståen af efterårssemestret 3. sem. 2020:  
•	Oeait19eiA: 15  
•	Oeait19eiB: 15  

12.	Sammenholde lecture fagplaner for koordinering  
OutPut: Justeringer så vores emner understøtter hinanden.  
•	ALLE: Vi udfylder før mødet matrix på google docs.  

13.	Status på praktik  
OutPut: Eventuelle justeringer og dokumentopfølgning?  
•	RUTR: Orienterer.  

14.	Projekter i foråret 2020  
OutPut: Plan for hvordan vi får udarbejdet en ”procedure” for samarbejde med virksomheder.  
•	RUTR præsenterer igangværende projekter.  
•	RUTR: Orienterer. Hvilke krav vi har til virksomheder.  

15.	Team budget  
OutPut: Ønsker til budget et beløb.  
•	RUTR: Orienterer.  

16.	Valgfag i efteråret  
OutPut: En plan for hvornår vi skaber valgfagskataloget og hvem der er ansvarlig.  
•	ALLE: ?  

17.	IoT virksomheder  
Output: Plan for hvordan vi kommer videre med arbejdet med virksomheder og de nye teknologier inden for IoT.  
•	NISI: Orienterer.  
•	PDA: Orienterer om Uddannelsesudvalget.  

18.	ITT team arbejdsgang  
Output: Beslutning om formen på opgavekoordinering.  
•	PDA: Hvem er eksamenskoordinator efter Morten efter juni 2020?  

19.	Team corner ~ vores nye pladser i den renoverede lærerlounge.  
OutPut: Beslutning om hvor vi gerne vil sidde. Ønske til Marie.  
•	RUTR: Orientere og begrunder.  


22.	Eventuelt  
•	Alle: 

23.	Næste møde.  
•	PDA indkalder.  
