---
title: 'IT Technology Team Meeting Agenda'
subtitle: 'Agenda'
authors: ['Per Dahlstrøm \<pda@ucl.dk>']
main_author: 'Per Dahlstrøm'
date: \today
email: 'pda@ucl.dk'
left-header: \today
right-header: 'IT Technology Team Meeting Agenda'
---

# Læsere

Undervisere og ledere.

---

# Formål

1. At opliste de opgaver der skal varetages og opliste emner der skal gennemsnakkes som grundlag for beskrivelse og evt. uddelegering af opgaver der skal varetages.  
2. Danne grundlag for et referat der oplister opgaver der skal varetages med en deadline og en ansvarlig.

---

<div style="page-break-after: always; visibility: hidden"> 
\pagebreak 
</div>

2020-04-02 Kl.: 12:30 til 15:00. ITT Teammødeagenda. MFNY, HJM, NISI, RUTR, ILES, MON, PDA

# Klasserepræsentanter: Bliver indkaldt.  

1.
*	2. sem. A: Jens VandeVeire	jens365y@edu.ucl.dk  
Substitute: Nicolai Svarer	Skytte	nico835t@edu.ucl.dk
Good lecture organisation in these online times.  
It is hard to keep motivated.  
 More group work would be good. Maybe some online room.  
The mood in class is a very mixed bag.  
It is imortant to keep a 
A lot of time spen/wasted om setting up OrCad. Ilias: Unexpected issues.
Mandatory OrCad course i 1. semester.




* 2. sem. B:   
Substitut: Nikita	Morozovs	niki1396@edu.ucl.dk
* 4. Sem. A: Nicolai Lyngs Jensen, nico429s@edu.ucl.dk  
Substitute
*	4. Sem. B:  
Substitute: Daniel Mohr Maslygan Lilleballe, dani7378@edu.ucl.dk

2.	Referat fra sidste møde  
Output: Beslutninger der skal følges op på.  
*	PDA: Status på beslutninger og beslutninger der skal følges op på:  
    *  	Lecture plans  
    *	Employability – Jobparathed  
    *	Klasserne og Obligatoriske LæringsAktiviteter  
    *	Eksamen juni 2020 Eksamen ITT2 og ITT4  
    *	Vi skal bemande åbent hus lørdag d. 22. februar fra 11:00 til 15:00.  
    *	Studerende der har været til samtale PIEC deltager måske.  
    *  	Budgettal for studerende 2019 efterår til 2020 efterår. Dog ikke september optaget 2020.
    *	Sammenholde lecture fagplaner for koordinering  
    *	Status på praktik  
    *	Projekter i foråret 2020  
    *	Team budget  
    *	IoT virksomheder  
    *	Team corner ~ vores nye pladser i den renoverede lærerlounge.  

3.	Klasse B?  
Output: En plan for klasse B?  
•	ALLE: Hvad kan vi gøre?

1. Fou: Vores oplæg fra 5. og 6. marts Afdelingsseminar drøftes med ledelsen.
OutPut: Plan for videre arbejde.
•	MFNY/Hans: Spørgsmål og tanker om videre arbejde.

3.	Hvor meget er materialebeløbet på uddannelsen?  
Output: Vi skal have skrevet det samlede materialebeløb på ucl.dk incl. Bøger.  
•	PDA: Hver især gør op hvad der skal bruges i deres fag.  
•	PDA: Får det samlede beløb skrevet på ucl.dk.   

4. Robotbrag.
Output: Begrundet indstilling til deltagelse eller ikke deltagelse.
•	PDA: Orienterer.  

1. Matlab  
Output: Hvis vi tager til Århus expo og hvordan vi bruge matlab. Beslutning om hvilket fokus vi har for matlab.  
•	RUTR: Orienterer.  

1. Praktik.  
Output: Konsensus om hvordan studerende informeres om praktikken før og under praktikken.  
•	RUTR: Orienterer.  
3 - 4 studerende skal lave skoleprojekt.


16.	Valgfag i efteråret.
OutPut: En plan for hvornår vi skaber valgfagskataloget og hvem der er ansvarlig.  
•	ILES: Er ansvarlig.

8.	Kommende eksaminer. (Hvem er koordinator?)  
MFNY: Hvilken eksamensform regner vi med? Fysisk eller Online?  
Der er ikke nogen afklaring på formen. MC, praktisk prøve.  
Vi forsøger at dække pensum og substiture det der ikke kan lade sig gøre med andet relevant.  
Blended MC test med praktisk arbejde for at finde løsning.

22.	Eventuelt  
•	Alle: 

23.	Næste møde.  
•	PDA indkalder.  
